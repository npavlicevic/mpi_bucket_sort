#include "mpi_vector.h"
main(int argc, char **argv) {
  int my_rank;
  int p;
  int n;
  int n_bar;
  float local_v[MAX_LOCAL_ORDER];
  float bucket_v[MAX_LOCAL_ORDER];
  float rejects_v[MAX_LOCAL_ORDER];
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  fscanf(stdin, "%d", &n);
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
  n_bar = n / p;
  mpi_read_vector_scatter(local_v, n_bar, p, my_rank);
  mpi_print_vector(local_v, n_bar, p, my_rank);
  mpi_bucket_rejects(rejects_v, local_v, n_bar, my_rank);
  mpi_print_vector(local_v, n_bar, p, my_rank);
  mpi_print_vector(rejects_v, n_bar, p, my_rank);
  mpi_bucket_take_rejects(local_v, rejects_v, n_bar, p, my_rank);
  mpi_print_vector(local_v, n_bar, p, my_rank);
  mpi_bucket_sort(bucket_v, local_v, n_bar, my_rank);
  mpi_bucket_print(bucket_v, n_bar, p, my_rank);
  MPI_Finalize();
}
