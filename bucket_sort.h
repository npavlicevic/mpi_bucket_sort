#ifndef BUCKET_SORT_H
#define BUCKET_SORT_H
#include "mpi_vector.h"
void mpi_bucket_sort(float bucket_v[], float local_v[], int n_bar, int my_rank);
void mpi_bucket_rejects(float rejects_v[], float local_v[], int n_bar, int my_rank);
void mpi_bucket_take_rejects(float local_v[], float rejects_v[], int n_bar, int p, int my_rank);
void mpi_bucket_print(float bucket_v[], int n, int p, int my_rank);
#endif
