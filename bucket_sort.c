#include "bucket_sort.h"
void mpi_bucket_sort(float bucket_v[], float local_v[], int n_bar, int my_rank) {
  int i, val;
  for(i = 0; i < n_bar; i++) {
    val = (int)local_v[i] - (my_rank * n_bar);
    bucket_v[val] = 1.0;
  }
}
void mpi_bucket_rejects(float rejects_v[], float local_v[], int n_bar, int my_rank) {
  int i, j, start_at = my_rank * n_bar, end_at = start_at + n_bar, val;
  for(i = 0; i < n_bar; i++) {
    rejects_v[i] = -1.0;
  }
  for(i = 0, j = 0; i < n_bar; i++) {
    if(local_v[i] < start_at || local_v[i] > end_at) {
      val = (int)local_v[i];
      local_v[i] = -1.0;
      rejects_v[j++] = val;
    }
  }
}
void mpi_bucket_take_rejects(float local_v[], float rejects_v[], int n_bar, int p, int my_rank) {
  float _rejects_v[MAX_LOCAL_ORDER];
  MPI_Allgather(rejects_v, n_bar, MPI_FLOAT, _rejects_v, n_bar, MPI_FLOAT, MPI_COMM_WORLD);
  int i, j, start_at = my_rank * n_bar, end_at = start_at + n_bar;
  for(i = 0, j = 0; i < n_bar * p; i++) {
    if(_rejects_v[i] < 0.0) {
      continue;
    }
    if(_rejects_v[i] < start_at) {
      continue;
    }
    if(_rejects_v[i] > end_at) {
      continue;
    }
    for(; j < n_bar; j++) {
      if(local_v[j] < 0.0) {
        break;
      }
    }
    local_v[j] = _rejects_v[i];
  }
}
void mpi_bucket_print(float bucket_v[], int n, int p, int my_rank) {
  int i;
  float _bucket_v[MAX_LOCAL_ORDER];
  MPI_Gather(bucket_v, n, MPI_FLOAT, _bucket_v, n, MPI_FLOAT, 0, MPI_COMM_WORLD);
  if(!my_rank) {
    for(i = 0; i < n * p; i++) {
      if(_bucket_v[i] == 1.0) {
        printf("%d ", i);
      }
    }
    printf("\n");
  }
}
