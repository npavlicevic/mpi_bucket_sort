#
# Makefile 
# bucket sort
#

CC=mpicc
LIBS=-lmpi -lmpivector
FILES=bucket_sort.c main.c
FILES_RANGE=bucket_sort.c main_range.c
REMOVE=main main_range

all: main range

main: ${FILES}
	${CC} $^ -o main ${LIBS}

range: ${FILES_RANGE}
	${CC} $^ -o main_range ${LIBS}

clean: ${REMOVE}
	rm $^
